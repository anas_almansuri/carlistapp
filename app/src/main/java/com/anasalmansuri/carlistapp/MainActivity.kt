package com.anasalmansuri.carlistapp

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_carlist.*
import okhttp3.*
import java.io.IOException
import java.io.Serializable

class MainActivity : AppCompatActivity() {
lateinit var carlistObject : carList
override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_carlist)
        recyclerViewMain.layoutManager = LinearLayoutManager(this)
        goToMap.isEnabled=false
       fetchJSON()
        goToMap.setOnClickListener {
                sendDataToMapActivity(carlistObject)
        }
    }
    //Fetching Json Data from the API end point and sending it to the recyclerview adapter
     fun fetchJSON(){
        val url = "https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json"
        val request = Request.Builder().url(url).build()
        val client = OkHttpClient()
        var progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        Handler().postDelayed({progressDialog.dismiss()}, 5000)
        client.newCall(request).enqueue(object: Callback {
            override fun onResponse(call: Call?, response: Response?) {
                val body = response?.body()?.string()
                val gson = GsonBuilder().create()
                carlistObject = gson.fromJson(body, carList::class.java)
                runOnUiThread{
                    progressDialog.dismiss()
                    goToMap.isEnabled=true
                    recyclerViewMain.adapter = MainAdapter(carlistObject)
                }
            }
            override fun onFailure(call: Call?, e: IOException?) {

            }
        })
    }

    //Sending data to the map activity
    fun sendDataToMapActivity(carlist : carList){
        val i = Intent(this@MainActivity, MapActivity::class.java)
        i.putExtra("carlist", carlist)
        startActivity(i)
        }
    }



class carList(val placemarks: List<PlaceMark>) : Serializable

class PlaceMark(val address: String, val name: String, val coordinates: ArrayList<Double>, val engineType: String, val exterior: String, val fuel: Int, val interior: String, val vin: String) : Serializable
