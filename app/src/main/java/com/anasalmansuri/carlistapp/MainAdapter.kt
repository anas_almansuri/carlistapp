package com.anasalmansuri.carlistapp

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.car_row.view.*

class MainAdapter(val cars: carList): RecyclerView.Adapter<CustomViewHolder>(){

    //Number of Cars
    override fun getItemCount(): Int {
        return cars.placemarks.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.car_row, parent,false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        //Display Cars on Recycler View
        val carObj = cars.placemarks[position]
        holder.view.carNameTextView.text = carObj.name
        holder.view.carAddressTextView.text= carObj.address
        holder.view.carLatTextView.text= carObj.coordinates[0].toString()
        holder.view.carLngTextView.text= carObj.coordinates[1].toString()
        holder.view.engineTypeTextView.text= carObj.engineType
        holder.view.exteriorTextView.text= carObj.exterior
        holder.view.interiorTextView.text= carObj.interior
        holder.view.fuelTextView.text= carObj.fuel.toString()
        holder.view.vinTextView.text= carObj.vin

    }
}

class CustomViewHolder(val view: View): RecyclerView.ViewHolder(view){

}